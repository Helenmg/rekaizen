import aja from 'aja'

const ADD_GOOD_PRACTICE = '#add-good-practice'
const ADD_LESSON_LEARNED = '#add-lesson-learned'
const ADD_NEW_GOOD_PRACTICE_BUTTON = '#new-good-practice'
const ADD_NEW_LESSON_LEARNED = '#new-lesson-learned'
const ADD_NEW_POSITIVE = '#new-positive'
const POSITIVE_DESCRIPTION = '#positive-description'
const ADD_NEW_NEGATIVE = '#new-negative'
const NEGATIVE_DESCRIPTION = '#negative-description'
const ISSUE_PATH = '/'

class Issue {
  constructor() {
    cy.visit(ISSUE_PATH)

    cy.get(ADD_NEW_GOOD_PRACTICE_BUTTON)
    cy.get(ADD_NEW_LESSON_LEARNED)
    cy.get(ADD_NEW_POSITIVE)
    cy.get(ADD_NEW_NEGATIVE)
  }

  openNewGoodPractice() {
    cy.get(ADD_NEW_GOOD_PRACTICE_BUTTON).click()

    return this
  }

  openNewLessonLearned() {
    cy.get(ADD_NEW_LESSON_LEARNED).click()

    return this
  }

  openNewPositive() {
    cy.get(ADD_NEW_POSITIVE).click()

    return this
  }

  openNewNegative() {
    cy.get(ADD_NEW_NEGATIVE).click()

    return this
  }

  fillGoodPracticeWith(text) {
    cy.get('#good-practice-text').type(text)

    return this
  }

  fillLessonLearnedWith(text) {
    cy.get('#lesson-learned-text').type(text)

    return this
  }

  fillPositiveWith(description) {
    cy.get(POSITIVE_DESCRIPTION).type(description)

    return this
  }

  fillNegativeWith(description) {
    cy.get(NEGATIVE_DESCRIPTION).type(description)

    return this
  }

  confirmDeclaration() {
    cy.get('#confirm-declaration').click()

    return this
  }

  confirmPositiveWithEnter() {
    cy.get(POSITIVE_DESCRIPTION).type('{enter}')

    return this
  }

  confirmNegativeWithEnter() {
    cy.get(NEGATIVE_DESCRIPTION).type('{enter}')

    return this
  }

  addGoodPractice() {
    cy.get(ADD_GOOD_PRACTICE).click()

    return this
  }

  addLessonLearned() {
    cy.get(ADD_LESSON_LEARNED).click()

    return this
  }

  withAGoodPractice(description) {
    aja()
    .method('post')
    .body({description: description})
    .url('http://api:3001/createGoodPractice')
    .go()

    return this
  }

  withALessonLearned(description) {
    aja()
    .method('post')
    .body({description: description})
    .url('http://api:3001/createLessonLearned')
    .go()

    return this
  }

  includes(text) {
    try {
      cy.contains(text)

      return true
    } catch(error) {
      if (!(error instanceof CypressError)) { throw error }

      return false
    }
  }

  canAddGoodPractice() {
    try {
      cy.get(ADD_GOOD_PRACTICE).should('have.attr', 'disabled')

      return false
    } catch(error) {
      if (!(error instanceof CypressError)) { throw error }

      return true
    }
  }

  canAddLessonLearned() {
    try {
      cy.get(ADD_LESSON_LEARNED).should('have.attr', 'disabled')

      return false
    } catch(error) {
      if (!(error instanceof CypressError)) { throw error }

      return true
    }
  }
}

export default Issue

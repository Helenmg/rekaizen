import Fixtures from './Fixtures'
import Issue from './pages/Issue'

describe('Rekaizen', () => {
  beforeEach(() => {
    Fixtures.cleanCollections()
  })

  afterEach(() => {
    Fixtures.cleanCollections()
  })

  it('creates a Lesson Learned', () => {
    const description = "lesson learned text"

    const issue = new Issue()
      .openNewLessonLearned()
      .confirmDeclaration()
      .fillLessonLearnedWith(description)
      .addLessonLearned()

    expect(issue.includes('¡Habéis añadido una Lección Aprendida!')).to.be.true
  })

  it('can not add an undeclared Lesson Learned', () => {

    const issue = new Issue()
      .openNewLessonLearned()

    expect(issue.canAddLessonLearned()).to.be.false
  })

  it('shows a Lesson Learned list', () => {
    const aLessonLearned = 'first'

    const issue = new Issue().withALessonLearned(aLessonLearned)

    expect(issue.includes(aLessonLearned)).to.be.true
  })
})

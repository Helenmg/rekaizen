import React from 'react'
import Column from '../components/Column'

class GoodPractices extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      showModal: false
    }
  }

  showModal() {
    this.setState({showModal: true})
  }

  hideModal() {
    this.setState({showModal: false})
  }

  render() {
    return(
      <Column
        id={"good-practice"}
        add={this.props.translations.add}
        cancel={this.props.translations.cancel}
        explanation={this.props.translations.goodPracticeExplanation}
        modalTitle={this.props.translations.addGoodPractice}
        confirmation={this.props.translations.goodPracticeConfirmation}
        listTitle={this.props.translations.goodPractices}
        openModal={this.showModal.bind(this)}
        showModal={this.state.showModal}
        hideModal={this.hideModal.bind(this)}
        create={this.props.create}
        collection={this.props.goodPractices}
      />
    )
  }
}

export default GoodPractices

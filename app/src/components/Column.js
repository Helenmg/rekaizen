import React from 'react'
import List from './List'
import Modal from './Modal'

class Column extends React.Component {
  render() {
    return (
      <div className="col-25">
        <div className="card-block">
          <Modal {...this.props}/>
          <List {...this.props} onClick={this.props.openModal}/>
        </div>
      </div>
    )
  }
}

export default Column

import ButtonAdd from './ButtonAdd'
import React from 'react'

class List extends React.Component {
  rows(){
    let rows = []
    this.props.collection.forEach((item, index) =>{
      if(item.description != '' && item.description != undefined ){
        rows.push(
          <tr key={index}>
            <td colspan="2" className="white-td">
              <p className="padding-left">{item.description}</p>
            </td>
          </tr>
        )
      }
    })
    return rows
  }

  render() {
    return (
      <div>
        <div className="header-list">
          <div class="left">
            <h5>{this.props.listTitle}</h5>
          </div>
          <div class="right">
            <ButtonAdd
              onClick={this.props.onClick}
              text={this.props.add}
              id={this.props.id}
            />
          </div>
        </div>
        <table className="white-table" id={this.props.id + "-list"}>
          <tbody>
            {this.rows()}
          </tbody>
        </table>
      </div>
    )
  }
}

List.defaultProps = {
  collection: [],
  id: '',
  listTitle: '',
  onClick: () => {},
  add: ''
}

export default List

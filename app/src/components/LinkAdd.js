import React from 'react'

class LinkAdd extends React.Component {
  render() {
    return (
      <a className="link" onClick={this.props.onClick} id={"new-" + this.props.id}>{this.props.text}</a>
    )
  }
}

LinkAdd.defaultProps = {
  id: '',
  onClick: () => {},
  text: ''
}

export default LinkAdd

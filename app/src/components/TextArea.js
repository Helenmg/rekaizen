import React from 'react'

class TextArea extends React.Component {
  onKeyPress(event) {
    if(event.key == "Escape"){this.props.hide()}

    if(event.key == "Enter"){
      this.props.create(event.target.value)
      this.cleanTextarea(event.target)
      event.preventDefault()
    }
  }

  cleanTextarea(target) {
    target.value = ""
  }

  onBlur() {
    this.props.hide()
  }

  render() {
    if (!this.props.show) {return (<div></div>)}

    return (
      <textarea
        className="white-textarea"
        id={this.props.id + "-description"}
        onBlur={this.onBlur.bind(this)}
        onKeyDown={this.onKeyPress.bind(this)}
        autoFocus="true"
        tabindex="-1"
      />
    )
  }
}

TextArea.defaultProps = {
  create: () => {},
  hide: () => {},
  show: false
}

export default TextArea

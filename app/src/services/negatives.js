import {Bus} from '../infrastructure/bus'
import {APIClient} from '../infrastructure/apiClient'


export default class Negatives {
  constructor() {
    this.client = APIClient

    this.subscriptions()
  }

  subscriptions() {
    Bus.subscribe('create.negative', this.create.bind(this))
    Bus.subscribe('retrieve.negatives', this.retrieveAllFor.bind(this))
    Bus.subscribe('remove.negative', this.removeNegative.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.negative')
    let body = payload
    let url = 'createNegative'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallback('retrieved.negatives')
    let body = payload
    let url = 'retrieveNegatives'
    this.client.hit(url, body, callback)
  }

  removeNegative(payload) {
    let callback = this.buildCallback('removed.negative')
    let body = payload
    let url = 'removeNegative'
    this.client.hit(url, body, callback)
  }


  buildCallback(signal) {
    return function(response) {
      Bus.publish(signal, response)
    }
  }
}

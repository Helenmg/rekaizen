import {Bus} from '../infrastructure/bus'
import {APIClient} from '../infrastructure/apiClient'


export default class GoodPractices {
  constructor() {
    this.client = APIClient

    this.subscriptions()
  }

  subscriptions() {
    Bus.subscribe('create.goodPractice', this.create.bind(this))
    Bus.subscribe('retrieve.goodPractices', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.goodPractice')
    let body = payload
    let url = 'createGoodPractice'
    this.client.hit(url, body, callback)
  }

  retrieveAllFor(payload){
    let callback = this.buildCallback('retrieved.goodPractices')
    let body = payload
    let url = 'retrieveGoodPractices'
    this.client.hit(url, body, callback)
  }

  buildCallback(signal) {
    return function(response) {
      Bus.publish(signal, response)
    }
  }
}

import aja from 'aja'

export let APIClient = {
  URL: process.env.API_URL,

  hit: function (endpoint, data, action){
    let base_url = this.URL
    aja()
    .method('post')
    .body(data)
    .url(base_url + endpoint)
    .on('success', action)
    .go();
  }
}

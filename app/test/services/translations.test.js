import Translations from '../../src/services/translations'
import Subscriber from './subscriber'
import {Bus} from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Translations Service', () => {
  it('retrieves all the translations', () => {
    const subscriber = new Subscriber('got.translations')
    const service = new Translations()
    service.client = new StubAPI()

    service.retrieveTranslations()

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

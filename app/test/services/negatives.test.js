import Negatives from '../../src/services/negatives'
import Subscriber from './subscriber'
import {Bus} from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Negatives Service', () => {
  it('creates a negative', () => {
    const subscriber = new Subscriber('created.negative')
    const service = new Negatives()
    service.client = new StubAPI()

    Bus.publish('create.negative', { description: 'A Negative' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the negatives', () => {
    const subscriber = new Subscriber('retrieved.negatives')
    const service = new Negatives()
    service.client = new StubAPI()

    Bus.publish('retrieve.negatives')

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

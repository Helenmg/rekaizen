import GoodPractices from '../../src/services/goodPractices'
import Subscriber from './subscriber'
import {Bus} from '../../src/infrastructure/bus'
import StubAPI from './stubApi'

describe('Good Practices Service', () => {
  it('creates a good practice', () => {
    const subscriber = new Subscriber('created.goodPractice')
    const service = new GoodPractices()
    service.client = new StubAPI()

    Bus.publish('create.goodPractice', { description: 'A Good Practice' })

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })

  it('retrieves the good practices', () => {
    const subscriber = new Subscriber('retrieved.goodPractices')
    const service = new GoodPractices()
    service.client = new StubAPI()

    Bus.publish('retrieve.goodPractices')

    expect(subscriber.hasBeenCalled).toBeTruthy()
  })
})

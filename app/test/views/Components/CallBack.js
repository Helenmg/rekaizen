class Callback {
  constructor() {
    this.called = false
  }

  toBeCalled(){
    this.called = true
  }

  hasBeenCalled() {
    return this.called
  }
}

export default Callback

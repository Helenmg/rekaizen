import Modal from '../../../src/components/Modal'
import {Bus} from '../../../src/infrastructure/bus'
import TestRenderer from 'react-test-renderer'
import React from 'react'

describe('Modal', () => {
  it('starts hidden', () => {
    const modal = TestRenderer.create(<Modal />).getInstance()

    expect(modal.state.show).toBeFalsy()
  })


  it('starts with add button disabled', () => {
    const modal = TestRenderer.create(<Modal />).getInstance()

    expect(modal.state.disabled).toBeTruthy()
  })

  it('enables the button add when is confirmed', () => {
    const modal = TestRenderer.create(<Modal />).getInstance()

    modal.confirm()

    expect(modal.state.disabled).toBeFalsy()
  })
})

import TextArea from '../../../src/components/TextArea'
import Subscriber from '../../services/subscriber'
import {Bus} from '../../../src/infrastructure/bus'
import TestRenderer from 'react-test-renderer'
import Callback from './CallBack'
import React from 'react'

describe('TextArea', () => {
  it('cleans itself when press Enter', () => {
    const textArea = TestRenderer.create(<TextArea />).getInstance()
    const event = {key: 'Enter', target: {value: 'text'}, preventDefault: ()=>{}}

    textArea.onKeyPress(event)

    expect(event.target.value).toEqual('')
  })

  it('execute a callback when press Enter', () => {
    const callBack = new Callback()
    const positive = 'positive'
    const textArea = TestRenderer.create(<TextArea create={callBack.toBeCalled.bind(callBack)}/>).getInstance()
    const event = {key: 'Enter', target: {value: positive}, preventDefault: ()=>{}}

    textArea.onKeyPress(event)

    expect(callBack.hasBeenCalled()).toBeTruthy()
  })

  it('executes a collback when pres Escape', () => {
    const callBack = new Callback()
    const positive = 'positive'
    const textArea = TestRenderer.create(<TextArea hide={callBack.toBeCalled.bind(callBack)}/>).getInstance()
    const event = {key: 'Escape', preventDefault: ()=>{}}

    textArea.onKeyPress(event)

    expect(callBack.hasBeenCalled()).toBeTruthy()
  })
})

import SimpleList from '../../../src/components/SimpleList'
import TestRenderer from 'react-test-renderer'
import Callback from './CallBack'
import React from 'react'

describe('SimpleList', () => {
  it('prints a collection', () => {
    const textToPrint = "description"
    const collection = [{description: textToPrint}]

    const list = TestRenderer.create(<SimpleList collection={collection}/>).toJSON()

    expect(content(list)).toEqual(textToPrint)
  })

  it('prints nothing for an empty collection', () => {
    const emptyCollection = []

    const list = TestRenderer.create(<SimpleList collection={emptyCollection}/>).toJSON()

    expect(contentForEmpty(list)).toBeNull()
  })

  it('calls removes an element', () => {
    const event = {preventDefault: () => {}}
    const callBack = new Callback()
    const list = TestRenderer.create(<SimpleList remove={callBack.toBeCalled.bind(callBack)}/>).getInstance()

    list.onClick('item', event)

    expect(callBack.hasBeenCalled()).toBeTruthy()
  })

  function contentForEmpty(list){
    return list.children[0].children[1].children
  }

  function content(list){
    return list.children[0].children[1].children[0].children[0].children[1].children[0]
  }
})

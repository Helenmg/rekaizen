class TranslationsCollection {
  static retrieveAll () {
    return {
      rekaizen: "ReKaizen",
      goodPractices: "Buenas Prácticas",
      addGoodPractice: "Añadir Buena Práctica",
      cancel: "Cancelar",
      add: "Añadir",
      goodPracticeAdded: "¡Habéis añadido una Buena Practica!",
      goodPracticeExplanation: "Una Buena Práctica debe ser consensuada por toda la squad y ser formulada de manera positiva y específica. Expresa la validación de que una práctica ha demostrado un impacto positivo en la Squad que queremos mantener en el tiempo.",
      goodPracticeConfirmation: "Como squad declaramos la voluntad de mantener esta Buena Práctica.",
      lessonsLearned: "Lecciones Aprendidas",
      addLessonLearned: "Añadir Lección Aprendida",
      lessonLearnedAdded: "¡Habéis añadido una Lección Aprendida!",
      lessonLearnedExplanation: "Una Lección Aprendida debe ser consensuada y declarada por toda la squad. Se formula de manera específica, preferentemente con un enfoque positivo, y sostenible en el tiempo. Expresa la adquisición de un aprendizaje significativo surgido de un análisis previo.",
      lessonLearnedConfirmation: "Como squad declaramos que está Lección Aprendida forma parte de nuestro conocimiento adquirido a través de nuestra experiencia.",
      positives: "Positivos",
      addPositive: "+ Añadir positivo",
      negatives: "Negativos",
      addNegative: "+ Añadir negativo"
    }
  }
}

module.exports = TranslationsCollection;

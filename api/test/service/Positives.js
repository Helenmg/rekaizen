const expect = require('chai').expect

const PositivesService = require ('../../positives/service')
const PositivesCollection = require ('../../positives/collection')
const Positive = require ('../../domain/Positive')


describe('Positives service', () => {
  beforeEach(()=>{
    PositivesCollection.drop()
  })
  it('retrieves Positives in descendent order', () =>{
    const firstPositive = new Positive()
    const secondPositive = new Positive()
    secondPositive.birthday += 10000
    PositivesCollection.create(firstPositive)
    PositivesCollection.create(secondPositive)

    let retrieved = PositivesService.retrieveAll()

    expect(retrieved[0].birthday).to.eq(firstPositive.birthday)
  })

  it('retrieves Positives for an issue', ()=>{
    const description = 'a description'
    const issue = 'an issue'
    const positiveForAnIssue = new Positive(description, issue)
    const positiveForAnotherIssue = new Positive('another description', 'another issue')
    PositivesCollection.create(positiveForAnIssue)
    PositivesCollection.create(positiveForAnotherIssue)

    let retrieved = PositivesService.retrieveAll(issue)

    expect(retrieved.length).to.eq(1)
    expect(retrieved[0].description).to.eq(description)
  })

  it('removes a Positive from positive list', ()=>{
    const description = 'a description'
    const issue = 'an issue'
    const positiveForAnIssue = new Positive(description, issue)
    const secondPositiveForAnIssue = new Positive('second description', issue)
    const thirdPositiveForAnIssue = new Positive('third description', issue)
    const fourthPositiveForAnIssue = new Positive('fourth description', issue)
    PositivesCollection.create(positiveForAnIssue)
    PositivesCollection.create(secondPositiveForAnIssue)
    PositivesCollection.create(thirdPositiveForAnIssue)
    PositivesCollection.create(fourthPositiveForAnIssue)
    secondPositiveForAnIssue.birthday += 10000
    thirdPositiveForAnIssue.birthday += 20000
    fourthPositiveForAnIssue.birthday += 30000

    let removed = PositivesService.remove(thirdPositiveForAnIssue)
    let retrieved = PositivesService.retrieveAll(issue)

    expect(retrieved.length).to.eq(3)
    expect(retrieved[0].description).to.eq(positiveForAnIssue.description)
    expect(retrieved[1].description).to.eq(secondPositiveForAnIssue.description)
    expect(retrieved[2].description).to.eq(fourthPositiveForAnIssue.description)
  })
})

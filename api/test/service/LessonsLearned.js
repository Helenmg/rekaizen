const expect = require('chai').expect

const LessonsLearnedService = require ('../../lessonsLearned/service')
const LessonsLearnedCollection = require ('../../lessonsLearned/collection')
const LessonLearned = require ('../../domain/LessonLearned')


describe('Lessons Learned service', () => {
  beforeEach(()=>{
    LessonsLearnedCollection.drop()
  })

  it('retrieves Lessons Learned in ascendent order', () =>{
    const firstLessonLearned = new LessonLearned()
    const secondLessonLearned = new LessonLearned()
    secondLessonLearned.birthday += 10000
    LessonsLearnedCollection.create(firstLessonLearned)
    LessonsLearnedCollection.create(secondLessonLearned)

    let retrieved = LessonsLearnedService.retrieveAll()

    expect(retrieved[0].birthday).to.eq(secondLessonLearned.birthday)
  })

  it('retrieves Lessons Learned for an issue', () =>{
    const description = 'a description'
    const issue = 'an issue'
    const lessonLearnedForAnIssue = new LessonLearned(description, issue)
    const lessonLearnedForAnotherIssue = new LessonLearned('another description', 'another issue')
    LessonsLearnedCollection.create(lessonLearnedForAnIssue)
    LessonsLearnedCollection.create(lessonLearnedForAnotherIssue)

    let retrieved = LessonsLearnedService.retrieveAll(issue)

    expect(retrieved.length).to.eq(1)
    expect(retrieved[0].description).to.eq(description)
  })
})

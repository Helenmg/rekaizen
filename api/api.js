const TranslateAll = require('./actions/TranslateAll')
const CreateGoodPractice = require('./actions/CreateGoodPractice')
const CreatePositive = require('./actions/CreatePositive')
const CreateNegative = require('./actions/CreateNegative')
const CreateLessonLearned = require('./actions/CreateLessonLearned')
const RetrieveGoodPractices = require('./actions/RetrieveGoodPractices')
const RetrieveLessonsLearned = require('./actions/RetrieveLessonsLearned')
const RetrievePositives = require('./actions/RetrievePositives')
const RetrieveNegatives = require('./actions/RetrieveNegatives')
const RemovePositive = require('./actions/RemovePositive')
const RemoveNegative = require('./actions/RemoveNegative')

const GoodPracticesCollection = require('./goodPractices/collection')
const LessonsLearnedCollection = require('./lessonsLearned/collection')
const PositivesCollection = require('./positives/collection')
const NegativesCollection = require('./negatives/collection')

const express = require('express')
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const api = express()
var router = express.Router();
const cors = require('cors');


api.use(cors());
api.options('*', cors());
api.use(logger('dev'));
api.use(express.json());
api.use(express.urlencoded({ extended: false }));
api.use(cookieParser());

router.post('/translate', function (req, res) {
  const message = TranslateAll.do()

  res.send(JSON.stringify(message))
})

router.post('/createGoodPractice', function (req, res) {
  const goodPractice = req.body
  const message = CreateGoodPractice.do(goodPractice)

  res.send(JSON.stringify(message))
})

router.post('/retrieveGoodPractices', function (req, res) {
  const issue = req.body
  const message = RetrieveGoodPractices.do(issue.issue)

  res.send(JSON.stringify(message))
})

router.post('/createLessonLearned', function (req, res) {
  const lessonLearned = req.body
  const message = CreateLessonLearned.do(lessonLearned)

  res.send(JSON.stringify(message))
})

router.post('/retrieveLessonsLearned', function (req, res) {
  const issue = req.body
  const message = RetrieveLessonsLearned.do(issue.issue)

  res.send(JSON.stringify(message))
})

router.post('/createPositive', function (req, res) {
  const positive = req.body
  const message = CreatePositive.do(positive)

  res.send(JSON.stringify(message))
})

router.post('/retrievePositives', function (req, res) {
  const issue = req.body
  const message = RetrievePositives.do(issue.issue)

  res.send(JSON.stringify(message))
})

router.post('/removePositive', function (req, res) {
  const positive = req.body
  const message = RemovePositive.do(positive)

  res.send(JSON.stringify(message))
})

router.post('/createNegative', function (req, res) {
  const negative = req.body
  const message = CreateNegative.do(negative)

  res.send(JSON.stringify(message))
})

router.post('/retrieveNegatives', function (req, res) {
  const issue = req.body
  const message = RetrieveNegatives.do(issue.issue)

  res.send(JSON.stringify(message))
})

router.post('/removeNegative', function (req, res) {
  const negative = req.body
  const message = RemoveNegative.do(negative)

  res.send(JSON.stringify(message))
})


router.post('/clean', function (req, res) {
  GoodPracticesCollection.drop()
  LessonsLearnedCollection.drop()
  PositivesCollection.drop()
  NegativesCollection.drop()
})

api.use('/', router);

module.exports = api;

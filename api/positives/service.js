const PositivesCollection = require ('./collection')
const Positive = require ('../domain/Positive')

class PositivesService {
  static create(description, issue) {
    let positive = new Positive(description, issue)
    return PositivesCollection.create(positive)
  }

  static retrieveAll(issue) {
    let allPositives = PositivesCollection.retrieveAll(issue)
    let ordered = this.orderYourself(allPositives)
    let serialized = ordered.map((aPositive) => {
      return aPositive.serialize()
    })
    return serialized
  }

  static remove(document) {
    let positive = Positive.from(document)

    let removedPositive = PositivesCollection.remove(positive)

    return removedPositive.serialize()
  }

  static orderYourself(positives) {
    return positives.sort((a, b) => {
      return b.isOlderThan(a)
    })
  }
}

module.exports = PositivesService

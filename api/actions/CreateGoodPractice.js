const GoodPracticesService = require ('../goodPractices/service')

class CreateGoodPractice {
  static do(goodPractice) {
    return GoodPracticesService.create(goodPractice.description, goodPractice.issue)
  }
}

module.exports = CreateGoodPractice

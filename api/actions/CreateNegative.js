const NegativesService = require ('../negatives/service')

class CreateNegative {
  static do(negative) {
    return NegativesService.create(negative.description, negative.issue)
  }
}

module.exports = CreateNegative

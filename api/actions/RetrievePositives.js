
const PositivesService = require ('../positives/service')

class RetrievePositives {
  static do(issue) {
    return PositivesService.retrieveAll(issue)
  }
}

module.exports = RetrievePositives

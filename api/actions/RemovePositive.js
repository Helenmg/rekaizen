const PositivesService = require ('../positives/service')

class RemovePositive {
  static do(positive) {
    return PositivesService.remove(positive)
  }
}

module.exports = RemovePositive

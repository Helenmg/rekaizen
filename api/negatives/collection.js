
let collection = []
class NegativesCollection {
  static create(negative) {
    collection.push(negative)

    return negative
  }

  static retrieveAll(issue) {
    const filtered = collection.filter((negative)=>{
      return negative.belongsTo(issue)
    })
    return filtered
  }

  static remove(negative) {
    let found = collection.find((element) => {
      return negative.isEqualTo(element)
    })

    let index = collection.indexOf(found)
    collection.splice(index, 1)

    return found
  }

  static drop() {
    collection = []
  }
}

module.exports = NegativesCollection

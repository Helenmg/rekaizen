const NegativesCollection = require ('./collection')
const Negative = require ('../domain/Negative')

class NegativesService {
  static create(description, issue) {
    let negative = new Negative(description, issue)
    return NegativesCollection.create(negative)
  }

  static retrieveAll(issue) {
    let allNegatives = NegativesCollection.retrieveAll(issue)
    let ordered = this.orderYourself(allNegatives)
    let serialized = ordered.map((aNegative) => {
      return aNegative.serialize()
    })
    return serialized
  }

  static remove(document) {
    let negative = Negative.from(document)

    let removedNegative = NegativesCollection.remove(negative)

    return removedNegative.serialize()
  }

  static orderYourself(negatives) {
    return negatives.sort((a, b) => {
      return b.isOlderThan(a)
    })
  }
}

module.exports = NegativesService

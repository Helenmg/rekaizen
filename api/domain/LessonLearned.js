
class LessonLearned {
  constructor(description, issue){
    this.birthday = new Date().getTime()
    this.description = description
    this.issue = issue
  }

  serialize() {
    return {
      'description': this.description,
      'issue': this.issue,
      'birthday': this.birthday
    }
  }

  isOlderThan(lessonLearned) {
    if(!(lessonLearned instanceof LessonLearned)){throw new Error('Can not be compared with another type')}

    return lessonLearned.birthday > this.birthday
  }

  belongsTo(issue) {
    return (this.issue == issue)
  }
}

module.exports = LessonLearned

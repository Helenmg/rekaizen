
class Positive {
  constructor(description, issue){
    this.birthday = new Date().getTime()
    this.description = description
    this.issue = issue
  }

  static from(document) {
    const positive = new Positive(
      document.description,
      document.issue)

    positive.birthday = document.birthday

    return positive
  }

  serialize() {
    return {
      'description': this.description,
      'issue': this.issue,
      'birthday': this.birthday
    }
  }

  isOlderThan(positive) {
    if(!(positive instanceof Positive)){throw new Error('Can not be compared with another type')}

    return positive.birthday > this.birthday
  }

  isEqualTo(positive) {
    return positive.birthday == this.birthday && positive.issue == this.issue
  }

  belongsTo(issue) {
    return (this.issue == issue)
  }
}

module.exports = Positive

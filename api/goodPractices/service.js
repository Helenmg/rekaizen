const GoodPracticesCollection = require ('./collection')
const GoodPractice = require ('../domain/GoodPractice')

class GoodPracticesService {
  static create(description, issue) {
    let goodPractice = new GoodPractice(description, issue)
    return GoodPracticesCollection.create(goodPractice)
  }

  static retrieveAll(issue) {
    let allGoodPractices = GoodPracticesCollection.retrieveAll(issue)
    let ordered = this.orderYourself(allGoodPractices)
    let serialized = ordered.map((aGoodPractice) => {
      return aGoodPractice.serialize()
    })
    return serialized
  }

  static orderYourself(goodPractices) {
    return goodPractices.sort((a, b) => {
      return a.isOlderThan(b)
    })
  }
}

module.exports = GoodPracticesService

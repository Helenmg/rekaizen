
let collection = []
class GoodPracticesCollection {
  static create(goodPractice) {

    collection.push(goodPractice)

    return goodPractice
  }

  static retrieveAll(issue) {
    const filtered = collection.filter((goodPractice)=>{
      return goodPractice.belongsTo(issue)
    })
    return filtered
  }

  static drop() {
    collection = []
  }
}

module.exports = GoodPracticesCollection

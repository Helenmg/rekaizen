const LessonsLearnedCollection = require ('./collection')
const LessonLearned = require ('../domain/LessonLearned')

class LessonsLearnedService {
  static create(description, issue) {
    let lessonLearned = new LessonLearned(description, issue)
    return LessonsLearnedCollection.create(lessonLearned)
  }

  static retrieveAll(issue) {
    let allLessonsLearned = LessonsLearnedCollection.retrieveAll(issue)
    let ordered = this.orderYourself(allLessonsLearned)
    let serialized = ordered.map((aLessonsLearned) => {
      return aLessonsLearned.serialize()
    })
    return serialized
  }

  static orderYourself(lessonsLearned) {
    return lessonsLearned.sort((a, b) => {
      return a.isOlderThan(b)
    })
  }
}

module.exports = LessonsLearnedService

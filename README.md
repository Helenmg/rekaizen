#Rekaizen

## Development

### Development dependencies

- `Docker version 18.06.1-ce` or higher.
- `docker-compose version 1.18.0` or higher.

### Running the application

You only need to run the following command in the project folder:

`docker-compose up --build`

> Remember to shutdown the docker conatiner with `docker-compose down` when you done your work.

### Running for development.

After run the application like is explained before you have to run in a new terminal:

`docker-compose exec app npm run build`

> Remember to run this command everytime you want to watch your changes or use `docker-compose exec app npm run build-watch` for rebuild for everytime you do changes in js files.
> IMPORTANT!!! Remeber to stop the watch before run the end to end tests.

## Tests

### Running the application test

After run the application you have to execute:

- `docker-compose exec app npm run test-all`

If you only want to run the unit component test, you have to run:

- `docker-compose exec app npm run test-unit`

If you only want to run the end to end test, you have to run:

- `docker-compose exec app npm run test-e2e`

> If it show a error where says: `Please reinstall Cypress by running: cypress install` before run the test you have to use: `docker-compose exec app npx cypress install`

### Running the api test

- `docker-compose exec api npm run test-all`

### Running all the tests

After run the application you have to execute:

- `sh run-all-tests.sh`

## Deploy

### Deploying to Demo

Actually we have two machines in Heroku for deploy the application for do demos, you can visit it in:

APP: `https://rekaizen.herokuapp.com`
API: `https://rekaizen-api.herokuapp.com`

> If you are in a Demo and want to reset the application data, you have to do a `POST` to `https://rekaizen-api.herokuapp.com/clean`

### Requirements for deploy

You need to have an account in heroku and permission for deploy in the two machines mentioned above and you have to have installed the `Heroku toolbelt`.

### How to deploy

#### API

One you are in the project folder, you have to enter in API folder, then you have to run the following commands:

`heroku login`
`git add .`
`git commit -m "<ANY MESSAGE>"`
`heroku git:remote -a rekaizen-api`
`git push heroku master -f`

#### APP

One you are in the project folder, you have to enter in APP folder, then you have to run the following commands:

`heroku login`
`git add .`
`git commit -m "<ANY MESSAGE>"`
`heroku git:remote -a rekaizen`
`git push heroku master -f`


#### Deploy all by script

Once you are in the project folder, has to do a `heroku login`, then after do the login you can run the following script:

- `sh deploy-to-demo.sh`
